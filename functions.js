function randomizeLetter(){
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var randomLetter = possible.charAt(Math.floor(Math.random() * possible.length));
    return randomLetter;
}

function getCountFromVal(arr, index, val)
{
  if (arr[index] != undefined) {
    if(arr[index].font === val)
    {
      return true;
    }
  };
}

function checkIfLetterIs(array, val)
{
  if (val !== '\b' || val !== '' || val !== ' ' || val !== ',' || val !== '.') {
    if (array.length != 0) {
      for(var i = 0; i < array.length; i++) {
          if (array[i] === val) {
            return true;
          };
      }
      return false;
    }else{
      return false;
    };
  };
}

function checkLastFont(arr) {
  if (arr.length != 0) {
    var lastItem = arr.length-1;
    return arr[lastItem].variant;
  }
}

function checkTransformedLetter(array, font) {
  if (array.length != 0) {
    for(var i = 0; i < array.length; i++) {
        if (array[i].variant === font) {
          return true;
        };
    }
    return false;
  }
}

function getVariantFont(array, font) {
  for(var i = 0; i < array.length; i++) {
      if (array[i].font === font) {
        return array[i].variant;
      };
  }
}

function getVariantFontSortedPhrase(array, font) {
  for(var i = 0; i < array.length; i++) {
      if (array[i].font === font) {
        return array[i].variant;
      };

      if (' ' === font) {
        return ' ';
      };

      if (',' === font) {
        return ',';
      };

      if ('.' === font) {
        return '.';
      };
  }
}

function getIndexOf(array, key){
  for(var i = 0; i < array.length; i++) {
     if(array[i].font === key) {
       return i;
     }
  }
}

function getValueOf(array, key){
  for(var i = 0; i < array.length; i++) {
     if(array[i].font === key) {
       return array[i].value;
     }
  }
}

function getNumeratorOf(array, key){
  for(var i = 0; i < array.length; i++) {
     if(array[i].font === key) {
       return array[i].index;
     }
  }
}

function countNumerOfLetters(array){
  var fontNumerator = array.length;
  return fontNumerator;
}

// Chronometer
var start = 0;
var end = 0;
var diff = 0;
var timerID = 0;
function chrono(){
  end = new Date();
  diff = end - start;
  diff = new Date(diff);
  var msec = diff.getMilliseconds();
  var sec = diff.getSeconds();
  var min = diff.getMinutes();
  if (min < 10){
    min = "0" + min;
  }
  if (sec < 10){
    sec = "0" + sec;
  }
  if(msec < 10){
    msec = "00" + msec;
  }
  else if(msec < 100){
    msec = "0" + msec;
  }
  document.getElementById("chronotime").innerHTML = min + ":" + sec + ":" + msec
  timerID = setTimeout("chrono()", 10)
}
function chronoStart(){
  start = new Date()
  chrono()
}
function chronoStop(){
  clearTimeout(timerID)
}
// Chronometer End

// Sounds Library
var acurrateSound = new Audio("assets/sounds/acurrate.mp3");
var errorSound = new Audio("assets/sounds/error.mp3");
var victorySound = new Audio("assets/sounds/victory.mp3");
// Sounds Library End