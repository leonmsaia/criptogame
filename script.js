    // INIT MODULES
    var cryphtogame = angular.module('cryphtogame', ['ngRoute', 'ngResource']);
    // INIT MODULES END

    // FACTORY
    
    // FACTORY END

    // MAIN CONTROLLER
    cryphtogame.controller("criptoMain", function($filter, $scope, $http) {

      // TEMPLATING
      $scope.pageTitle = 'Cripto Game';      
      // TEMPLATING END

      // Phrase Manager
      $scope.phraseInit = 'Desafiate a ti mismo con algo que sabes que nunca podrias hacer y con lo que encontrarias que puedes superar cualquier cosa.';
      //$scope.phraseInit = 'Tu tiempo es limitado, asi que no lo malgastes viviendo la vida de otra persona. No dejes que te atrape el dogma, que es vivir con los resultados de lo que otra persona piensa. No dejes que el ruido de las opiniones de los demas ahogue tu voz interior. Y lo mas importante, ten el coraje de seguir tu propio corazon y tu intuicion. Ellos saben de alguna manera lo que realmente quieres ser. Todo lo demas es secundario.';
      $scope.phraseInit = $scope.phraseInit.toUpperCase();
      $scope.phraseArray = $scope.phraseInit.split('');
      $scope.phraseObject = [];
      $scope.contenedor = [];
      $scope.contenedorSort = [];
      var numerador = 0;
      for (i = 0; i < $scope.phraseArray.length; i++) { 
        if ($scope.phraseArray[i] != ' ' && $scope.phraseArray[i] != ',' && $scope.phraseArray[i] != '.')
        {
          if (getCountFromVal($scope.contenedor, getIndexOf($scope.contenedor, $scope.phraseArray[i]), $scope.phraseArray[i]) == true) {
            var indexOfFont = getIndexOf($scope.contenedor, $scope.phraseArray[i]);
            var actualValueOfFont = getValueOf($scope.contenedor, $scope.phraseArray[i]);
            var numeratorOfFont = getNumeratorOf($scope.contenedor, $scope.phraseArray[i]);
            $scope.contenedor[indexOfFont] = {'font': $scope.phraseArray[i], 'index': numeratorOfFont, 'value': actualValueOfFont + 1};
          }else{
            var acumulador = {'font': $scope.phraseArray[i], 'index': numerador, 'value': 1}
            $scope.contenedor.push(acumulador);
            numerador++;
          };
        };
      }

      for (i = 0; i < $scope.phraseArray.length; i++) { 
          var modeladorLetraPhrasel =
          {
            letter:$scope.phraseArray[i],
            index:i,
          }
          $scope.phraseObject.push(modeladorLetraPhrasel);
      }
      // Phrase Manager End

      // Original Alphabeth
      $scope.alphabeth =[
           {font: 'A', key: 0},
           {font: 'B', key: 1},
           {font: 'C', key: 2},
           {font: 'D', key: 3},
           {font: 'E', key: 4},
           {font: 'F', key: 5},
           {font: 'G', key: 6},
           {font: 'H', key: 7},
           {font: 'I', key: 8},
           {font: 'J', key: 9},
           {font: 'K', key: 10},
           {font: 'L', key: 11},
           {font: 'M', key: 12},
           {font: 'N', key: 13},
           {font: 'O', key: 14},
           {font: 'P', key: 15},
           {font: 'Q', key: 16},
           {font: 'R', key: 17},
           {font: 'S', key: 18},
           {font: 'T', key: 19},
           {font: 'U', key: 20},
           {font: 'V', key: 21},
           {font: 'W', key: 22},
           {font: 'X', key: 23},
           {font: 'Y', key: 24},
           {font: 'Z', key: 25},
      ]
      // Original Alphabeth End

      // Sorted Alphabeth Generator
      $scope.alphabethTransformed =[];
      var indexTransformAlph = 0;
      var maximoBucle = $scope.alphabeth.length;
      var i = 0;
      while(i < maximoBucle) {
        if ($scope.alphabethTransformed.length != 0) {
          var randomLetterSaved = randomizeLetter();
          if (checkTransformedLetter($scope.alphabethTransformed, randomLetterSaved) == false) {
            var alphabethRandomItem = {font: $scope.alphabeth[i].font, index: indexTransformAlph, variant: randomLetterSaved, key: $scope.alphabeth[i].key};
            $scope.alphabethTransformed.push(alphabethRandomItem);
            indexTransformAlph++;
            i++;
          }
        }else{
          var randomLetterSaved = randomizeLetter();
          var alphabethRandomItem = {font: $scope.alphabeth[i].font, index: indexTransformAlph, variant: randomLetterSaved, key: $scope.alphabeth[i].key};
          $scope.alphabethTransformed.push(alphabethRandomItem);
          indexTransformAlph++;
          i++;
        }
      }
      // Sorted Alphabeth Generator End

      // Render Texts
      $scope.contenedorSort;
      for (i = 0; i < $scope.contenedor.length; i++) {
        var variantFont = getVariantFont($scope.alphabethTransformed, $scope.contenedor[i].font);
        var acumuladorSortedValue = {font: variantFont, index:$scope.contenedor[i].index, value:$scope.contenedor[i].value, original: $scope.contenedor[i].font};
        $scope.contenedorSort.push(acumuladorSortedValue);
      }
      $scope.phraseArraySort = [];
      for (i = 0; i < $scope.phraseObject.length; i++) {
        var fontConverted = getVariantFontSortedPhrase($scope.alphabethTransformed, $scope.phraseObject[i].letter);
        var acumuladorSortedValue = {font: fontConverted, index:$scope.phraseObject[i].index, value:$scope.phraseObject[i].value, original: $scope.phraseObject[i].letter};
        $scope.phraseArraySort.push(acumuladorSortedValue);
      }

      for (i = 0; i < $scope.phraseArraySort.length; i++) {
        
        if ($scope.phraseArraySort[i].font == '.' || $scope.phraseArraySort[i].font == ',' || $scope.phraseArraySort[i].font == '' || $scope.phraseArraySort[i].font == ' ') {
          var encriptedlElement = '<span class="letterVariant">' 
          + $scope.phraseArraySort[i].font
          + '</span>';
          $('.fraseMix').append(encriptedlElement);
        }else{
          var encriptedlElement = '<span class="letterVariant">' 
          + $scope.phraseArraySort[i].font
          
          + '<input maxlength="1" class="insertField" type="text" letter="' + $scope.phraseArraySort[i].font + '" valueID="'
          + $scope.phraseArraySort[i].index + '" />'
          
          + '</span>';
          $('.fraseMix').append(encriptedlElement);
        }

      }
      // Render Texts End

      // Checker Font
      $scope.validLettersInput = [];
      $scope.invalidLettersInput = [];
      
      var cantNumerosSort = countNumerOfLetters($scope.phraseArraySort);
      $('.fraseMix .letterVariant .insertField').on('input', function() {   
        var valorVolatil = $(this).val();
        valorVolatil = valorVolatil.toUpperCase();
        var valorVolatilID = $(this).attr('valueid');
        var valorFontOriginal = $(this).attr('letter');
        var originalFont = $scope.phraseObject[valorVolatilID].letter;
        originalFont = originalFont.toUpperCase();
        
        // Validation of Null Caracters
        if (valorVolatil != '' || valorVolatil != ' ' || valorVolatil != ',' || valorVolatil != '.') {
          // Comparactive of Chars with Original Font
          if (valorVolatil == originalFont) {           
            $('input[letter="' + valorFontOriginal + '"]').val(valorVolatil);
            // Sound Trigger (Acurrate)
            acurrateSound.play();
            // Sound Trigger (Acurrate) End

            // Add Correct Letter to Correct Letter List
            $scope.validLettersInput.push(valorVolatil);
            // Add Correct Letter to Correct Letter List End

            // Win Meter
            var lettersCorrectCounter = $scope.validLettersInput.length;
            var lettersCorrectMax = $scope.contenedorSort.length;
            if (lettersCorrectCounter == lettersCorrectMax) {
              chronoStop();
              victorySound.play();
            };
            // Win Meter End

          }else{
         
            $('input[letter="' + valorFontOriginal + '"]').val(valorVolatil);
            var checkIfLetterIsArray = checkIfLetterIs($scope.invalidLettersInput, valorVolatil);
            if (checkIfLetterIsArray == false) {
              $scope.invalidLettersInput.push(valorVolatil);            
              $('input[letter="' + valorFontOriginal + '"]').val(valorVolatil);
              // Sound Trigger (Acurrate)
              errorSound.play();
              // Sound Trigger (Acurrate) End
            };

          };
          // Comparactive of Chars with Original Font End
        }
        //console.log($scope.invalidLettersInput);
        // Validation of Null Caracters End
      });
      // Checker Font End

      // Error Counters
      $('input.insertField').on('keyup', function() {
        var errorsNumbers = $scope.invalidLettersInput.length;
        // $("#amount").val(amount);
        $('#contErrors').val(errorsNumbers);
      })

      // Error Counters End

      // Chronometer Init
      var initValidator = 0;
      $('input.insertField').on( "click", function() {
        if (initValidator == 0) {
          chronoStart();
          initValidator ++;
        };
      });
      // Chronometer Init End

    });
    // MAIN CONTROLLER END