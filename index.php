<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cripto Game</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-resource.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-route.js"></script>
        <script src="functions.js"></script>
        <script src="script.js"></script>
        <link rel="stylesheet" href="assets/css/criptoGameV01.css">
    </head>

    <body ng-app="cryphtogame">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-12" ng-controller="criptoMain">
                                
                                <div class="row">
                                    <header class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-8">
                                                {{pageTitle}}
                                            </div>
                                            <div class="col-md-4">
                                                MENU PRINCIPAL
                                            </div>
                                        </div>
                                    </header>
                                    <section class="userArea col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">DATOS USER</div>
                                            <div class="col-md-6">PTS</div>
                                        </div>
                                    </section>
                                    <section class="gameContainer col-md-12">
                                        <div class="row">
                                            <div class="errorCounter col-md-12">
                                                <div class="row">
                                                    <span class="col-md-6">Errors: <input type="text" id="contErrors" value="0" disabled></span>
                                                    <span class="col-md-6">Tiempo: <span class="timer" id="chronotime">00:00:00</span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-12 fraseMix"></div>
                                            <div class="col-md-12 phraseAuthor">AUTOR</div>

                                        </div>
                                    </section>
                                    <section class="buttonsContainer col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input class="btn btn-primary" type="button" value="Pista">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input class="btn btn-primary" type="button" value="Rendirme">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input class="btn btn-primary" type="button" value="Contestar">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>             

                            </div>
                        
                        </div>

                    </div>
                </div>
            </div>
        </div>
        

    </body>
</html>